<?php

/**
 * @file
 * Admin functions for soshake.
 */

/**
 * Implementation of soshake_auto().
 */
function soshake_auto($form_id, $form_values) {
  global $theme_key, $base_url;
  if (!isset($form_values["input"]["hooks"])) {
    $hook = "";
  }
  else {
    $hook = $form_values["input"]["hooks"];
  }
  $options = array(
    "like"          => "Facebook 'like'" ,
    "tweet"         => "Twitter",
    "gplus"         => "Google +1",
    "pinit"         => "Pinterest 'pin-it'",
    "linkedin"      => "LinkedIn Share",
    "fanbox"        => "Facebook Page Fan Box",
    "fbconnect"     => "Facebook Connect",
  );
  $themes = list_themes();
  $theme_object = $themes[$theme_key];
  foreach ($themes as $theme) {
    if ($theme->status == 1 && $theme->name != $theme_object->name) {
      $active_theme = $theme;
    }
  }
  $form = array();
  $form["soshake-help"] = array(
    '#type' => 'item',
    '#title' => 'Insert easily Social Plugins on your website',
    '#description' => 'Choose simply on wich block of your website you want to display a social plugin.<br />You can see what "block" is aviaible on your theme : <a href="' . $base_url . '/admin/structure/block/demo/' . $active_theme->name . '#overlay-context=admin/config/soshake/autoinsert" target="_blank">See my theme\'s blocks</a>',
  );
  $hooks = array("0" => "-- Choose a hook --");
  foreach ($active_theme->info["regions"] as $hook => $name) {
    $hooks["soshake-" . $hook] = t($name);
  }
  $form["soshake-hooks"] = array(
    "#type"         => "select",
    "#title"        => t("Where to plug SoShake widgets"),
    "#options"      => $hooks,
    "#description"  => "Choose wich hook you want to plug SoShake widgets",
    "#suffix"        => '<script type="text/javascript"> jQuery(document).ready(function() { jQuery("#edit-hooks").change(function() { jQuery(".hookForm").hide(); jQuery("#soshake-widgets_" + jQuery(this).val()).show(); }); }); </script>',
  );
  foreach ($active_theme->info["regions"] as $hook => $name) {
    $form["soshake-widgets_$hook"] = array(
      '#type'         => 'checkboxes',
      '#title'        => t("Wich widget to install on <b>") . $hook . "</b>",
      '#options'      => $options,
      '#prefix'       => "<div id='soshake-widgets_$hook' style='display:none' class='hookForm'>",
      '#suffix'       => "</div>",
      '#default_value' => variable_get("soshake-widgets_$hook", array()),
    );
  }
  if (!isset($form_values["input"]["soshake-hooks"])) {
    $hook = FALSE;
  }
  else {
    $hook = $form_values["input"]["soshake-hooks"];
  }
  if ($hook) {
    $db = db_insert('block')
      ->fields(array(
        'status' => 1,
        'weight' => -8,
        'region' => $hook,
        'module' => "soshake",
        'delta' => "soshake-" . $hook,
        'theme' => $active_theme->name,
        'custom' => 0,
        'visibility' => 0,
        'pages' => '',
        'title' => '<none>',
        'cache' => -1,
      ))
      ->execute();
  }
  return system_settings_form($form);
}

/**
 * Implementation of soshake_manuel().
 */
function soshake_manuel() {
  global $base_url;
  $form = array();
  $form['like'] = array(
    '#type' => 'textfield',
    '#title' => t('To add the <b>"Like" button</b>, copy this where you want to see it:'),
    '#description' => "",
    '#default_value' => '<div class="up2-like"></div>',
  );
  $form['tweet'] = array(
    '#type' => 'textfield',
    '#title' => t('To add the <b>"Tweet" button</b>, copy this where you want to see it :'),
    '#description' => "",
    '#default_value' => '<div class="up2-tweet"></div>',
  );
  $form['gplus'] = array(
    '#type' => 'textfield',
    '#title' => t('To add the <b>"+1" button from Google</b>, copy this where you want to see it :'),
    '#description' => "",
    '#default_value' => '<div class="up2-gplus"></div>',
  );
  $form['linkedin'] = array(
    '#type' => 'textfield',
    '#title' => t('To add the <b>"Share" button from LinkedIn</b>, copy this where you want to see it :'),
    '#description' => "",
    '#default_value' => '<div class="up2-linkedin"></div>',
  );
  $form['pinit'] = array(
    '#type' => 'textfield',
    '#title' => t('To add the <b>"Pin it" button from Pinterest</b>, copy this where you want to see it :'),
    '#description' => "",
    '#default_value' => '<div class="up2-pinit"></div>',
  );
  $form['fanbox_diff'] = array(
    '#type' => 'textfield',
    '#title' => t('To display the Facebook Fanbox of your Facebook Page, copy this code where you want to see it :'),
    '#description' => 'By default, the Facebook Page displayed will be the one defined in your SoShake account, if not, define the url of your Facebook Page in the "URL" attribute',
    '#default_value' => '<div class="up2-fanbox" url="FACEBOOK_FAN_PAGE_URL"></div>',
    '#size' => 100,
  );
  $form['fbconnect'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook connect allow your visitors to create an account with one click, using their info from Facebook. To add this plugin, just copy this code where you want to see it :'),
    '#description' => "",
    '#default_value' => '<div class="up2-fbconnect" url="' . $base_url . '/soshake"></div>',
    '#size' => 100,
  );
  return $form;
}

/**
 * Implementation of soshake_layout().
 */
function soshake_layout() {
  $form = array();
  $form["help"] = array(
    '#type' => 'item',
    '#title' => 'SoShakes positions',
    '#description' => 'Esthetic parameters for positionning your widgets.',
  );
  $form["soshake-layout"] = array(
    '#type' => 'radios',
    '#options' => array("vertical" => "Vertical (widgets will be in a column)", "horizontal" => "Horizontal (widgets will be side by side)"),
    '#default_value' => variable_get("soshake-layout"),
  );
  $form["help2"] = array(
    '#type' => 'item',
    '#title' => 'SoShakes alignement',
    '#description' => '',
  );
  $form["soshake-align"] = array(
    '#type' => 'radios',
    '#options' => array(
      "left" => "Left",
      "center" => "Center",
      "right" => "Right",
    ),
    '#default_value' => variable_get("soshake-align"),
  );
  return system_settings_form($form);
}

/**
 * Implementation of soshake_account().
 */
function soshake_account() {
  global $base_url;
  $form = array();
  $client = json_decode(file_get_contents("http://soshake.com/api/me.json?url=$base_url"));
  if ($client->code == 200) {
    $content = '
    <p>
      Visit your SoShake account to define actions you want to display on your website when a content is shared.<br />
      <br />
      <a href="http://soshake.com/front" target="_blank" class="up2-button">Go to SoShake.com (link will open in a new window)</a>
    </p>
    ';
  }
  else {
    $content = '<div class="messages error"><p>You don\'t have a SoShake Account. This won\'t change the behavior of this module, but you won\'t benefit from every awesome feature we\'ve made !<br />
    <a href="http://soshake.com/?lang=en" target="_blank" class="up2-button">Create an account</a><p></div>';
  }
  $form['texte'] = array(
    '#type' => 'item',
    '#title' => t('Your SoShake Account'),
    '#description' => "<script type=\"text/javascript\" src=\"http://soshake.com/api/actions/cms/drupal.js\"></script>$content",
  );
  return ($form);
}
